# Qwirkle

Projet de fin d'année en Algorithmique en première année de prépa à l'EISTI. Règle du jeu Qwirkle : [regle en pdf](https://www.iello.fr/regles/Qwirkle_regles_FR_web.pdf)

## Pour jouer :
Lancez `./qwirkle` avec un (ou non) des paramètres suivant :
 - `-j` pour spécifier le nombre de joueurs humains et machine (ex : ooh pour 2 humains et 1 machine)
 - `-c` pour spécifier le nombre de couleurs (max 10)
 - `-f` pour spécifier le nombre de formes (max 10)
 - `-t` pour spécifier le nombre de tuiles (max 10)
 - `-g` pour lancer l'interface graphique

## Pour compiler
`fpc qwirkle.pas` suffit. Necessite fpc 3 ou sup.
